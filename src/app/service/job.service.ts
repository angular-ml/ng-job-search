import { HttpClient } from '@angular/common/http';
import { Injectable, } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Job, JobDetail } from '../model/job.model';

@Injectable({
  providedIn: 'root',
})
export class JobService {
  private apiUrl: string = '/jobs';

  constructor(private http: HttpClient) {}

  getJobs(): Observable<Job[]> {
    return this.http.get<Job[]>(this.apiUrl);
  }

  getFavoriteJobs(): Observable<Job[]> {
    const favoriteJobs = JSON.parse(localStorage.getItem('favoriteJobs') || '[]');
    return of(favoriteJobs);
  }

  getJobDetail(jobId: string): Observable<JobDetail> {
    return this.http.get<JobDetail>(`/jobs/${jobId}`);
  }
}
