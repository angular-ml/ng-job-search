import { TestBed } from '@angular/core/testing';

import { JobService } from './job.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('JobService', () => {
  let service: JobService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [JobService]
    });

    service = TestBed.inject(JobService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });


  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
