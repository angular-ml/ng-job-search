import { Component, OnInit, inject } from '@angular/core';
import { AsyncPipe, Location } from '@angular/common';
import { JobService } from '../../service/job.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EMPTY, Observable, catchError, switchMap, tap } from 'rxjs';
import { JobDetail } from '../../model/job.model';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
  selector: 'app-detail-job',
  standalone: true,
  imports: [AsyncPipe],
  templateUrl: './detail-job.component.html',
  styleUrl: './detail-job.component.css',
})
export class DetailJobComponent implements OnInit {
  private jobService: JobService = inject(JobService);
  private route: ActivatedRoute = inject(ActivatedRoute);
  private location: Location = inject(Location);
  private sanitizer: DomSanitizer = inject(DomSanitizer);
  private router: Router = inject(Router);
  job$!: Observable<JobDetail | null>;
  error: string | null = null;

  sanitizedDescription: SafeHtml | null = null;

  ngOnInit(): void {
    this.loadJobDetail();
  }

  private loadJobDetail(): void {
    this.job$ = this.route.paramMap.pipe(
      switchMap((params) => {
        const id = params.get('id');
        if (!id) {
          this.router.navigate(['/jobs']);
          return EMPTY;
        }
        return this.jobService.getJobDetail(id);
      }),
      tap((job: JobDetail | null) => {
        if (job) {
          this.sanitizedDescription = this.sanitizer.bypassSecurityTrustHtml(
            job.description
          );
        }
      }),
      catchError((error) => {
        console.error('Failed to load job details', error);
        this.router.navigate(['/']);
        return EMPTY;
      })
    );
  }

  goBack(): void {
    this.location.back();
  }
}
