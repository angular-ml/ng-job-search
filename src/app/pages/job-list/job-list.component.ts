import { Component, OnInit, inject, signal } from '@angular/core';
import { Job } from '../../model/job.model';
import { Observable, catchError, of, switchMap } from 'rxjs';
import { JobService } from '../../service/job.service';
import { AsyncPipe, CommonModule } from '@angular/common';
import { ActivatedRoute, RouterLink } from '@angular/router';

@Component({
  selector: 'app-job-list',
  standalone: true,
  imports: [CommonModule, AsyncPipe, RouterLink],
  templateUrl: './job-list.component.html',
  styleUrl: './job-list.component.css',
})
export class JobListComponent implements OnInit {

  private jobService: JobService = inject(JobService);
  private route: ActivatedRoute = inject(ActivatedRoute);

  jobs$!: Observable<Job[]>;
  favoriteJobs = signal<Job[]>([]);
  isFavoritesPage = false;
  detailRoute!: string;

  ngOnInit(): void {
    this.loadJobs();
    this.loadFavorites();
  }

  private loadJobs(): void {
    this.jobs$ = this.route.data.pipe(
      switchMap((data) => {
        this.isFavoritesPage = data['showFavorites'] ?? false;
        this.detailRoute = this.isFavoritesPage ? '/favorites/detail' : '/jobs/detail';

        return this.isFavoritesPage
          ? this.jobService.getFavoriteJobs()
          : this.jobService.getJobs();
      }),
      catchError(error => {
        console.error('Failed to load jobs', error);
        return of([]);
      })
    );
  }

  private loadFavorites(): void {
    const savedFavorites = localStorage.getItem('favoriteJobs');
    if (savedFavorites) {
      this.favoriteJobs.set(JSON.parse(savedFavorites));
    }
  }

  toggleFavorite(job: Job): void {
    if (!this.isFavoritesPage) {
      const favorites = this.favoriteJobs();
      const index = favorites.findIndex((fav) => fav.id === job.id);

      if (index === -1) {
        favorites.push(job);
      } else {
        favorites.splice(index, 1);
      }

      this.favoriteJobs.set([...favorites]);
      this.saveFavorites();
    }
  }

  private saveFavorites(): void {
    localStorage.setItem('favoriteJobs', JSON.stringify(this.favoriteJobs()));
  }

  isFavorite(job: Job): boolean {
    return this.favoriteJobs().some((fav) => fav.id === job.id);
  }
}
