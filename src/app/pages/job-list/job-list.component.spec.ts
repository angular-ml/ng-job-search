import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobListComponent } from './job-list.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { JobService } from '../../service/job.service';
import { of } from 'rxjs';

const jobServiceMock = {
  getJobs: () => of([]),
  getFavoriteJobs: () => of([])
};

describe('JobListComponent', () => {
  let component: JobListComponent;
  let fixture: ComponentFixture<JobListComponent>;
  let jobService: JobService;
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [JobListComponent, HttpClientTestingModule, RouterTestingModule],
      providers: [
        {JobService, useValue: jobServiceMock}
      ]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(JobListComponent);
    component = fixture.componentInstance;
    jobService = TestBed.inject(JobService);
    fixture.detectChanges();

    spyOn(jobService, 'getJobs').and.returnValue(of([]));

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
