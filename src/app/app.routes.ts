import { Routes } from '@angular/router';
import { NotFoundPageComponent } from './pages/not-found-page/not-found-page.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: '/jobs',
    pathMatch: 'full',
  },
  {
    path: 'jobs',
    loadComponent: () =>
      import('./pages/job-list/job-list.component').then(
        (m) => m.JobListComponent
      ),
  },
  {
    path: 'jobs/detail/:id',
    loadComponent: () =>
      import('./pages/detail-job/detail-job.component').then(
        (m) => m.DetailJobComponent
      ),
  },
  {
    path: 'favorites',
    loadComponent: () =>
      import('./pages/job-list/job-list.component').then(
        (m) => m.JobListComponent
      ),
      data: {
        showFavorites: true
      }
  },
  {
    path: 'favorites/detail/:id',
    loadComponent: () =>
      import('./pages/detail-job/detail-job.component').then(
        (m) => m.DetailJobComponent
      ),
  },
  { path: '**', component: NotFoundPageComponent },
];
